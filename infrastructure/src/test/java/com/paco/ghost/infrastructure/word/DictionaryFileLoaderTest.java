package com.paco.ghost.infrastructure.word;

import com.paco.ghost.domain.exception.DictionaryLoadingException;
import com.paco.ghost.domain.model.Dictionary;
import com.paco.ghost.test.BaseTest;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author paco
 */
public class DictionaryFileLoaderTest extends BaseTest {

    //SUT
    DictionaryFileLoader dictionaryFileLoader;
    //Aux
    private static final String MISNAMED_FILE_NAME = "Error.lst";
    private static final int FULL_DICTIONARY_SIZE = 173528;
    private static final int PURGED_DICTIONARY_SIZE = 43622;

    @Before
    public void setUp() {
        dictionaryFileLoader = new DictionaryFileLoader(new DictionaryPurger());
    }

    @Test
    public void loadMisnamedFileShouldRaiseException() {
        try {
            dictionaryFileLoader.loadFromFile(MISNAMED_FILE_NAME);
            Assert.fail("Exception should be raised");
        } catch (DictionaryLoadingException e) {
            assertThat(e, notNullValue());
        }
    }

    @Test
    public void loadShouldRead173528Words() {
        Dictionary dictionary = dictionaryFileLoader.loadFromFile(DictionaryFileLoader.DEFAULT_FILE_NAME);
        assertThat(dictionary.size(), equalTo(FULL_DICTIONARY_SIZE));
    }
    
    @Test
    public void loadAndPurgeShouldRead43622Words() {
        Dictionary dictionary = dictionaryFileLoader.load();
        assertThat(dictionary.size(), equalTo(PURGED_DICTIONARY_SIZE));
    }
}
