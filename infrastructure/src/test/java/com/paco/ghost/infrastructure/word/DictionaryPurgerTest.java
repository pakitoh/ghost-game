package com.paco.ghost.infrastructure.word;

import com.paco.ghost.domain.model.Dictionary;
import com.paco.ghost.domain.model.Word;
import com.paco.ghost.test.BaseTest;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author paco
 */
public class DictionaryPurgerTest extends BaseTest {

    //SUT
    DictionaryPurger dictionaryPurger;
    //Aux
    private static final int PURGED_DICTIONARY_SIZE = 13244;
    private Dictionary purgedDictionary;
    private Dictionary unpurgedDictionary;
    
    @Before
    public void setUp() {
        unpurgedDictionary = new Dictionary();
        unpurgedDictionary.add(new Word("aa"));
        unpurgedDictionary.add(new Word("aal"));
        unpurgedDictionary.add(new Word("aalii"));
        unpurgedDictionary.add(new Word("aaliis"));
        dictionaryPurger = new DictionaryPurger();        
        purgedDictionary = dictionaryPurger.purge(unpurgedDictionary);
    }

    @Test
    public void purgeShouldRemoveWordsAa() {
        assertThat(purgedDictionary.contains(new Word("aa")), is(false));
    }

    @Test
    public void purgeShouldRemoveWordAal() {
        assertThat(purgedDictionary.contains(new Word("aal")), is(false));
    }

    @Test
    public void purgeShouldNotRemoveWordAalii() {
        assertThat(purgedDictionary.contains(new Word("aalii")), is(true));
    }

    @Test
    public void purgeShouldRemoveWordAaliis() {
        assertThat(purgedDictionary.contains(new Word("aaliis")), is(false));
    }
}
