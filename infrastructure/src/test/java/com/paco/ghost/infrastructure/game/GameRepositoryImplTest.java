package com.paco.ghost.infrastructure.game;

import com.paco.ghost.domain.exception.GameNotFoundException;
import com.paco.ghost.domain.model.Game;
import com.paco.ghost.domain.model.GameId;
import com.paco.ghost.test.BaseTest;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author paco
 */
public class GameRepositoryImplTest extends BaseTest {
    
     //SUT
    private GameRepositoryImpl repository;
    //Aux
    private static final String ID = "1";
    
    @Before
    public void setUp() {        
        repository = new GameRepositoryImpl();
    }

    @Test
    public void addGameShouldIncrementTheSizeOfTheRepository() {        
        Game game = new Game(new GameId(ID), null, null);        
        assertThat(repository.getSize(), equalTo(0));
        repository.addGame(game);        
        assertThat(repository.getSize(), equalTo(1));
    }      
    
     @Test
    public void getANonExistingGameShouldRaiseException() {         
        try {
            Game game = repository.getGame(new GameId(ID));
            Assert.fail("Exception should be raised");
        } catch (GameNotFoundException e) {
            assertThat(e, notNullValue());
        }
    }

    @Test
    public void getAnExistingGameShouldReturnTheGameInTheRepository() {        
        Game preexistingGame = new Game(new GameId(ID), null, null);
        repository.addGame(preexistingGame);
        Game game = repository.getGame(preexistingGame.getId());
        assertThat(game, equalTo(preexistingGame));
    }  
}
