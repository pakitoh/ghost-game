package com.paco.ghost.infrastructure.word;

import com.paco.ghost.domain.model.Dictionary;
import com.paco.ghost.domain.model.Word;
import java.util.Iterator;
import javax.inject.Named;

/**
 *
 * @author paco
 */
@Named
public class DictionaryPurger {

    public static final int MINIMAL_LENGTH = 4;
    private Dictionary dictionary;

    public Dictionary purge(Dictionary dictionary) {
        this.dictionary = dictionary;
        purgeByLength();
        purgeBySubstrings();    
        return this.dictionary;
    }

    private void purgeByLength() {        
        for (Iterator<Word> itWords = dictionary.iterator(); itWords.hasNext();) {
            Word word = itWords.next();
            if(word.length() < MINIMAL_LENGTH) {
                itWords.remove();
            }
        }
    }

    private void purgeBySubstrings() {
        for (Iterator<Word> itWords = dictionary.iterator(); itWords.hasNext();) {
            Word word = itWords.next();                 
            boolean substringFound = false;
            for (int i = 1; i < word.length(); i++) {
                Word substring = word.substring(0, i);
                if (dictionary.contains(substring)) {
                    substringFound = true;
                }
            }
            if (substringFound) {
                itWords.remove();                
            }
        }
    }
}
