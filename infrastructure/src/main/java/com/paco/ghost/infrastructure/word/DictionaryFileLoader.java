package com.paco.ghost.infrastructure.word;

import com.paco.ghost.domain.exception.DictionaryLoadingException;
import com.paco.ghost.domain.model.Dictionary;
import com.paco.ghost.domain.model.Word;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.log4j.Logger;

/**
 *
 * @author paco
 */
@Named
public class DictionaryFileLoader {

    public static final String DEFAULT_FILE_NAME = "word.lst";
    private final Logger logger = Logger.getLogger(DictionaryFileLoader.class);
    private Dictionary dictionary;

    @Inject
    private final DictionaryPurger dictionaryPurger;

    public DictionaryFileLoader(DictionaryPurger dictionaryPurger) {
        this.dictionaryPurger = dictionaryPurger;
    }

    public Dictionary load() {
        loadFromFile(DEFAULT_FILE_NAME);
        return dictionaryPurger.purge(dictionary);
    }

    public Dictionary loadFromFile(String fileName) {
        dictionary = new Dictionary();

        BufferedReader reader = null;
        try {
            logger.debug("Try to load the file named " + fileName);
            URL resource = this.getClass().getClassLoader().getResource(fileName);
            if (resource == null) {
                throw new DictionaryLoadingException("Error loading Dictionary file");
            } else {
                String absolutePath = resource.getFile();
                logger.debug("Try to load the file in the path " + absolutePath);
                reader = new BufferedReader(new FileReader(new File(absolutePath)));
                while (reader.ready()) {
                    String line = reader.readLine();
                    logger.trace("Loading word  " + line);
                    dictionary.add(new Word(line));
                }
            }
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            throw new DictionaryLoadingException(e.getMessage(), e);
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
                logger.error(e.getMessage(), e);
                throw new DictionaryLoadingException(e.getMessage(), e);
            }
        }
        return dictionary;
    }
}
