package com.paco.ghost.infrastructure.game;

import com.paco.ghost.domain.exception.GameNotFoundException;
import com.paco.ghost.domain.model.Game;
import com.paco.ghost.domain.model.GameId;
import com.paco.ghost.domain.repository.GameRepository;
import java.util.HashMap;
import java.util.Map;

public class GameRepositoryImpl implements GameRepository {

    private final Map<GameId, Game> games;

    public GameRepositoryImpl() {
         games = new HashMap<GameId, Game>();
    }

    @Override
    public void addGame(final Game game) {
        games.put(game.getId(), game);
    }

    public int getSize() {
        return games.keySet().size();
    }

    @Override
    public Game getGame(final GameId gameId) {
        Game game = games.get(gameId);
        if (game == null) {
            throw new GameNotFoundException("Game with id " + gameId + " not found");
        }
        return game;
    }
}
