package com.paco.ghost.application;

import com.paco.ghost.domain.model.Game;
import com.paco.ghost.domain.model.GameId;
import com.paco.ghost.domain.model.Letter;
import com.paco.ghost.domain.factory.GameFactory;
import com.paco.ghost.domain.repository.GameRepository;
import com.paco.ghost.test.BaseMockTest;
import org.jmock.Expectations;
import static org.jmock.Expectations.returnValue;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author paco
 */
public class GameServiceTest extends BaseMockTest {

    //SUT
    private GameService gameService;
    //Collaborators
    private GameFactory factory;
    private GameRepository repository;
    //Aux
    private Game game;
    private final GameId gameId = new GameId("0e8702d1-00c2-47d5-8bff-1a65df5e1f7a");

    @Before
    public void setUp() {
        factory = context.mock(GameFactory.class);
        repository = context.mock(GameRepository.class);
        gameService = new GameService(factory, repository);
        game = context.mock(Game.class);
    }

    @Test
    public void createGameShouldCallGameFactory() {
        context.checking(new Expectations() {
            {
                oneOf(factory).createGame();
                will(returnValue(game));
                
                allowing(game).getId();
                
                oneOf(repository).addGame(game);
            }
        });
        game = gameService.createGame();
        context.assertIsSatisfied();
    }
  
    @Test
    public void addLetterShouldFindGameInRepositoryAndAddLetterToTheGame() {
        final Letter letter = new Letter('F');
        context.checking(new Expectations() {
            {
                oneOf(repository).getGame(gameId);
                will(returnValue(game));
                
                oneOf(game).addLetter(letter);
            }
        });
        gameService.addLetter(gameId, letter);
        context.assertIsSatisfied();
    }
    
    @Test
    public void getStateShouldCallCollaborators() {
        context.checking(new Expectations() {
            {
                oneOf(repository).getGame(gameId);
                will(returnValue(game));
                
                oneOf(game).getState();
            }
        });
        gameService.getState(gameId);
        context.assertIsSatisfied();
    }
    
    
    
}
