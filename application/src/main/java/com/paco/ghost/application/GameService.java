package com.paco.ghost.application;

import com.paco.ghost.domain.factory.GameFactory;
import com.paco.ghost.domain.model.Game;
import com.paco.ghost.domain.model.GameId;
import com.paco.ghost.domain.model.Letter;
import com.paco.ghost.domain.model.Word;
import com.paco.ghost.domain.repository.GameRepository;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.log4j.Logger;

/**
 *
 * @author paco
 */
@Named
public class GameService {

    private final Logger logger = Logger.getLogger(GameService.class);
    
    @Inject
    private final GameFactory factory;
    
    @Inject
    private final GameRepository repository;

    public GameService(final GameFactory factory, final GameRepository repository) {
        this.factory = factory;
        this.repository = repository;
    }

    public Game createGame() {
        Game game = factory.createGame();
        logger.debug("New game created with Id='" + game.getId() + "'");
        repository.addGame(game);
        return game;
    }

    public void addLetter(final GameId gameId, final Letter playerLetter) {
        Game game = repository.getGame(gameId);
        logger.debug("Player add letter '" + playerLetter.toChar() + "'");
        game.addLetter(playerLetter);
    }

    public Game.State getState(GameId gameId) {
        Game game = repository.getGame(gameId);
        logger.debug("Player request state of the game  '" + gameId + "'");
        return game.getState();        
    }

    public Word getWord(GameId gameId) {
        Game game = repository.getGame(gameId);
        logger.debug("Player request the word of the game  '" + gameId + "'");
        return game.getWord();        
    }

}
