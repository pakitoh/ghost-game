package com.paco.ghost.stories;

import com.paco.ghost.domain.model.Dictionary;
import com.paco.ghost.domain.model.Game;
import com.paco.ghost.domain.model.Letter;
import com.paco.ghost.domain.model.Word;
import com.paco.ghost.infrastructure.word.DictionaryFileLoader;
import com.paco.ghost.infrastructure.word.DictionaryPurger;
import com.paco.ghost.stories.util.ComponentFactory;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author paco
 */
public class PruebaFuncional {
     @Before
    public void setUp() {
    }
     
    
    public void testFindWordsStartingWith() {
        System.out.println("findWordsStartingWith");
        
        
        Word prefix = new Word("f");
        Dictionary dictionary =  new DictionaryFileLoader(new DictionaryPurger()).load();
        
        Dictionary result = dictionary.findWordsStartingWith(prefix);
        System.out.println("RESUL : " + result.toString());
    }
    
    
    
    @Test
    public void testAddLetter() {
        System.out.println("testAddLetter");
        
        ComponentFactory componentFactory = new ComponentFactory();
        Game game = componentFactory.getGameFactory().createGame();
        game.addLetter(new Letter('x'));
    }

    

}
