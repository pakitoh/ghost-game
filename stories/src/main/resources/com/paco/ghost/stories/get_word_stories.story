Narrative: Get game word
In order to think my next letter
As a player
I want to get the word of one game

Scenario: Try to get the word of a non existing game
Given a system ready to play
And the game id 8666f820-87ce-4a54-96b0-cefcde8a25c2
When the user request the word of the game
Then the system should inform that the game not exists
 
Scenario: Get the word of the new game
Given a system ready to play
And a new game
When the user request the word of the game
Then the word of the new game should be an empty word

Scenario: Get the word of the game when player wins
Given a system ready to play
And a game with the word balata
When the user request the word of the game
Then the word of the game should be balata

Scenario: Get the word of the game when system wins
Given a system ready to play
And a game with the word bacalao
When the user request the word of the game
Then the word of the game should be bacalao
