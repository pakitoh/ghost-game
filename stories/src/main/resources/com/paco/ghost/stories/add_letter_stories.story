Narrative: Add letter
In order to win the game
As a player
I want to add one letter to one game

Scenario: Add letter in a non existing game
Given a system ready to play
And the game id 8666f820-87ce-4a54-96b0-cefcde8a25c2
When the player add the letter F
Then the system should inform that the game not exists

Scenario: Add letter in a new game
Given a system ready to play
And a new game
When the player add the letter F
Then the first letter of the word should be F 
And the second letter of the word should exists
And the state of the game should be PLAYING

Scenario: Add one letter and the game go on
Given a system ready to play
And a new game
When the player add the letter F
Then the first letter of the word should be F 
And the second letter of the word should exists
And the state of the game should be PLAYING

Scenario: Add one letter and win
Given a system ready to play
And a game with the word bala
When the player add the letter t
Then the word should be balata
And the state of the game should be PLAYER_WIN

Scenario: Add one good letter and lose
Given a system ready to play
And a game with the word bacala
When the player add the letter o
Then the word should be bacalao
And the state of the game should be SYSTEM_WIN

Scenario: Add one wrong letter and lose
Given a system ready to play
And a game with the word bacala
When the player add the letter m
Then the word should be bacalam
And the state of the game should be SYSTEM_WIN

