Narrative: Get game state
In order to knows if I won
As a player
I want to get the state of one game

Scenario: Try to get the state of a non existing game
Given a system ready to play
And the game id 8666f820-87ce-4a54-96b0-cefcde8a25c2
When the user request the state of the game 
Then the system should inform that the game not exists

Scenario: Get the state of the new game
Given a system ready to play
And a new game
When the user request the state of the game 
Then the state of the game should be PLAYING

Scenario: Get the state of the game when player wins
Given a system ready to play
And a game with the word balata
When the user request the state of the game 
Then the state of the game should be PLAYER_WIN

Scenario: Get the state of the game when system wins
Given a system ready to play
And a game with the word bacalao
When the user request the state of the game 
Then the state of the game should be SYSTEM_WIN

