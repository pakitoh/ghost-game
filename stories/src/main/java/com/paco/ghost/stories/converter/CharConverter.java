package com.paco.ghost.stories.converter;

import org.jbehave.core.annotations.AsParameterConverter;

/**
 *
 * @author paco
 */
public class CharConverter {
    
    @AsParameterConverter
    public Character convert(String text) {
         return new Character(text.trim().toLowerCase().charAt(0));
    } 
}
