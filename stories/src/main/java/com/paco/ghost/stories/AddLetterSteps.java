package com.paco.ghost.stories;

import com.paco.ghost.application.GameService;
import com.paco.ghost.domain.exception.GameNotFoundException;
import com.paco.ghost.domain.model.Game;
import com.paco.ghost.domain.model.GameId;
import com.paco.ghost.domain.model.Letter;
import com.paco.ghost.stories.util.ComponentFactory;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import org.jbehave.core.annotations.BeforeScenario;
import org.jbehave.core.annotations.BeforeStory;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

/**
 *
 * @author paco
 */
public class AddLetterSteps {
 
    private ComponentFactory componentFactory;
    private GameService gameService;
    private GameId gameId;
    private Game game;
    private GameNotFoundException raisedException;
    
    @BeforeStory
    public void beforeStory() {
        componentFactory = new ComponentFactory();
    }
    
    @BeforeScenario
    public void beforeScenario() {
        gameService = null;
        game = null;
        raisedException = null;
    }
     
    @Given("a system ready to play")
    public void theSystemIsReadyToPlay() {        
        gameService = componentFactory.getApp();
    }

    @Given("the game id $gameId")
    public void theGameId(String gameId) {
        this.gameId = new GameId(gameId);
    }

    @Given("a new game")
    public void aNewGame() {
        game = gameService.createGame();
        gameId = game.getId();
    }

    @Given("a game with the word $word")
    public void aGameWithTheWord(String word) {
        game = gameService.createGame();
        gameId = game.getId();
        
        for (int i = 0; i < word.length(); i++) {
            if(i%2 == 0) { 
                gameService.addLetter(gameId, new Letter(word.charAt(i)));
            }
        }
    }
        
    @When("the player add the letter $playerLetter")
    public void thePlayerAddLetter(Character playerLetter) {
        try {
            gameService.addLetter(gameId, new Letter(playerLetter));
        } catch (GameNotFoundException e) {
            raisedException = e;
        }
    }

    @Then("the system should inform that the game not exists")
    public void theSystemShouldInformThatTheRequestedGameNotExists() {
        assertThat(raisedException, notNullValue());
    }

    @Then("the first letter of the word should be $firstLetter")
    public void theFirstLetterShouldBe(Character firstLetter) {
        assertThat(game.getWord().toString().charAt(0), equalTo(firstLetter));
    }

    @Then("the second letter of the word should exists")
    public void theSecondLetterShouldExists() {
        assertThat(game.getWord().length(), equalTo(2));
    }

    @Then("the state of the game should be $state")
    public void theStateShouldBe(String state) {
        assertThat(raisedException, nullValue());
        assertThat(game.getState().toString(), equalTo(state));
    }
    
    @Then("the word should be $word")
    public void theWordShouldBe(String word) {        
        assertThat(raisedException, nullValue());
        assertThat(game.getWord().toString(), equalTo(word));
    }
}
