package com.paco.ghost.stories;

import com.paco.ghost.application.GameService;
import com.paco.ghost.domain.exception.GameNotFoundException;
import com.paco.ghost.domain.model.Game;
import com.paco.ghost.domain.model.GameId;
import com.paco.ghost.domain.model.Letter;
import com.paco.ghost.stories.util.ComponentFactory;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import org.jbehave.core.annotations.BeforeScenario;
import org.jbehave.core.annotations.BeforeStory;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

/**
 *
 * @author paco
 */
public class GetStateSteps {
 
    private ComponentFactory componentFactory;
    private GameService gameService;
    private GameId gameId;
    private Game game;
    private GameNotFoundException raisedException;
    private Game.State state;
    
    @BeforeStory
    public void beforeStory() {
        componentFactory = new ComponentFactory();
    }
    
    @BeforeScenario
    public void beforeScenario() {
        gameService = null;
        game = null;
        raisedException = null;
    }
     
    @Given("a system ready to play")
    public void theSystemIsReadyToPlay() {        
        gameService = componentFactory.getApp();
    }

    @Given("the game id $gameId")
    public void theGameId(String gameId) {
        this.gameId = new GameId(gameId);
    }

    @Given("a new game")
    public void aNewGame() {
        game = gameService.createGame();
        gameId = game.getId();
    }

    @Given("a game with the word $word")
    public void aGameWithTheWord(String word) {
        game = gameService.createGame();
        gameId = game.getId();
        
        for (int i = 0; i < word.length(); i++) {
            if(i%2 == 0) { 
                gameService.addLetter(gameId, new Letter(word.charAt(i)));
            }
        }
    }
        
    @When("the user request the state of the game")
    public void theUserRequestTheState() {
        try {
            state = gameService.getState(gameId);
        } catch (GameNotFoundException e) {
            raisedException = e;
        }
    }

    @Then("the system should inform that the game not exists")
    public void theSystemShouldInformThatTheRequestedGameNotExists() {
        assertThat(raisedException, notNullValue());
    }

   
    @Then("the state of the game should be $state")
    public void theStateShouldBe(String state) {
        assertThat(raisedException, nullValue());
        assertThat(this.state.toString(), equalTo(state));
    } 
}
