package com.paco.ghost.stories.util;

import com.paco.ghost.application.GameService;
import com.paco.ghost.domain.factory.GameFactory;
import com.paco.ghost.domain.factory.IdFactory;
import com.paco.ghost.domain.model.Dictionary;
import com.paco.ghost.domain.model.Letter;
import com.paco.ghost.domain.model.Word;
import com.paco.ghost.domain.repository.GameRepository;
import com.paco.ghost.domain.service.NextLetterSelector;
import com.paco.ghost.domain.service.selector.PlayToLoseSelector;
import com.paco.ghost.domain.service.selector.PlayToWinSelector;
import com.paco.ghost.domain.service.selector.TieChooser;
import com.paco.ghost.domain.service.selector.WinnerCandidatesCalculator;
import com.paco.ghost.infrastructure.game.GameRepositoryImpl;
import com.paco.ghost.infrastructure.word.DictionaryFileLoader;
import com.paco.ghost.infrastructure.word.DictionaryPurger;
import java.util.Random;

/**
 *
 * @author paco
 */
public class ComponentFactory {

    private GameRepository gameRepository = null;
    private IdFactory idFactory = null;
    private GameFactory gameFactory = null;
    private GameService app = null;
    private Dictionary dictionary = null;
    private NextLetterSelector nextLetterSelector = null;
    
    public ComponentFactory() {      
        gameRepository = new GameRepositoryImpl();     
        idFactory = new IdFactory();
        TieChooser tieChooser = new TieChooser(new Random());
        dictionary = new DictionaryFileLoader(new DictionaryPurger()).load();
        nextLetterSelector = new NextLetterSelector(
                dictionary, 
                new WinnerCandidatesCalculator(), 
                new PlayToWinSelector(tieChooser), 
                new PlayToLoseSelector(tieChooser));
        gameFactory = new GameFactory(idFactory, dictionary, new FakeNextLetterSelector());
        app = new GameService(gameFactory, gameRepository);
    }

    public class FakeNextLetterSelector extends NextLetterSelector {

        public FakeNextLetterSelector() {
            super(null, null, null, null);            
        }
        
        public FakeNextLetterSelector(Dictionary dictionary, WinnerCandidatesCalculator candidatesCalculator, PlayToWinSelector playToWinSelector, PlayToLoseSelector loseSelector) {
            super(dictionary, candidatesCalculator, playToWinSelector, loseSelector);
        }

        @Override
        public Letter selectNextLetter(Word word) {
            return new Letter('a');
        }
    }

    public Dictionary getDictionary() {
        return dictionary;
    }
    
    public GameRepository getGameRepository() {
        return gameRepository;
    }

    public IdFactory getIdFactory() {
        return idFactory;
    }

    public GameFactory getGameFactory() {
        return gameFactory;
    }

    public GameService getApp() {
        return app;
    }
}
