package com.paco.ghost.stories;

import com.paco.ghost.application.GameService;
import com.paco.ghost.domain.model.Game;
import com.paco.ghost.stories.util.ComponentFactory;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import org.jbehave.core.annotations.BeforeStory;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

/**
 *
 * @author paco
 */
public class CreateGameSteps {

    private ComponentFactory componentFactory;
    private GameService gameService;
    private Game game;
            
    @BeforeStory
    public void beforeStory() {
        componentFactory = new ComponentFactory();
    }
    
    @Given("a system ready to play")
    public void theSystemIsReadyToPlay() {
        gameService = componentFactory.getApp();
    }
        
    @When("the user create a new game") 
    public void theUserCreateANewGame() { 
        game = gameService.createGame();        
    } 

    @Then("the system should create a new game")      
    public void theSystemShouldCreateANewGame() {       
        assertThat(game, notNullValue());
        assertThat(gameService.getState(game.getId()), equalTo(Game.State.PLAYING));
    }     
}
