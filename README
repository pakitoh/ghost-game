An implementation for the ghost-game 
http://en.wikipedia.org/wiki/Ghost_%28game%29

Optimal Ghost
In the game of Ghost, two players take turns building up an English word from left to right. Each player adds one letter per turn. The goal is to not complete the spelling of a word: if you add a letter that completes a word (of 4+ letters), or if you add a letter that produces a string that cannot be extended into a word, you lose. (Bluffing plays and "challenges" may be ignored for the purpose of this puzzle.)
Write a program that allows a user to play Ghost against the computer.
The computer should play optimally given the attached dictionary. Allow the human to play first. If the computer thinks it will win, it should play randomly among all its winning moves; if the computer thinks it will lose, it should play so as to extend the game as long as possible (choosing randomly among choices that force the maximal game length).
Your program should be written as a Java web application that provides a basic GUI for a human to play against the optimal computer player from inside the Firefox browser. The web page should make use of AJAX to update the page as the game progresses. You can use any Java web framework you like, or do it in pure servlet/JSP. You can also use any Javascript library you like, or use pure DOM browser features for the AJAX part.
Please submit your source code, configuration instructions, and any comments on your approach. Please also include a WAR file that we can test against Tomcat 5.5.x on Sun's J2SE 6.0. As an optional extra you can attempt to answer this question: if the human starts the game with 'n', and the computer plays according to the strategy above, what unique word will complete the human's victory?
While the core functionality of your solution is important it is also important to exhibit good structure. Criteria will include readability, maintainability, etc.
