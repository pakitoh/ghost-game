package com.paco.ghost.rest;

import com.paco.ghost.domain.model.GameId;
import com.paco.ghost.web.pojo.GameWeb;
import com.paco.ghost.web.rest.GameResource;
import com.paco.ghost.web.rest.GhostConfig;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.moxy.json.MoxyJsonFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.TestProperties;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

/**
 *
 * @author paco
 */
public class GameResourceTest extends JerseyTest {

    @Rule
    public TestWatcher watcher = new TestWatcher() {

        @Override
        protected void starting(Description description) {
            System.out.println("Starting test: " + description.toString());
        }
    };

    public GameResourceTest() {
    }

    @Override
    protected Application configure() {
        enable(TestProperties.LOG_TRAFFIC);
        enable(TestProperties.DUMP_ENTITY);
        return new ResourceConfig(GameResource.class);
    }

    @Test
    public void testCreateGame() {
        GameWeb gameWeb = target("game")                
                .register(MoxyJsonFeature.class)
                .register(GhostConfig.createMoxyJsonResolver())
                .request(MediaType.APPLICATION_JSON)
                .post(null, GameWeb.class);
        assertThat(gameWeb, notNullValue());
    }

    @Test
    public void testExecuteGetGame() {
        GameWeb gameWeb = target("game")                
                .register(MoxyJsonFeature.class)
                .register(GhostConfig.createMoxyJsonResolver())
                .request(MediaType.APPLICATION_JSON)
                .post(null, GameWeb.class);
        GameWeb gameWeb2 = target(gameWeb.getHref())
                .request(MediaType.APPLICATION_JSON)
                .get(GameWeb.class);
        assertThat(gameWeb2, notNullValue());
    }

    @Test
    public void testAddLetter() {
        GameWeb gameWeb = target("game")                
                .register(MoxyJsonFeature.class)
                .register(GhostConfig.createMoxyJsonResolver())
                .request(MediaType.APPLICATION_JSON)
                .post(null, GameWeb.class);
        GameWeb gameWeb2 = target(gameWeb.getHref() + "/letter/n")
                .request(MediaType.APPLICATION_JSON)
                .post(null, GameWeb.class);
        assertThat(gameWeb2, notNullValue());
    }

    @Test
    public void testAddLetter2() {
        GameWeb gameWeb = target("game")                
                .register(MoxyJsonFeature.class)
                .register(GhostConfig.createMoxyJsonResolver())
                .request(MediaType.APPLICATION_JSON)
                .post(null, GameWeb.class);
        GameWeb gameWeb2 = target(gameWeb.getHref() + "/letter/n")
                .request(MediaType.APPLICATION_JSON)
                .post(null, GameWeb.class);
        GameWeb gameWeb3 = target(gameWeb.getHref() + "/letter/t")
                .request(MediaType.APPLICATION_JSON)
                .post(null, GameWeb.class);
        assertThat(gameWeb2, notNullValue());
    }

    
    
    
}
