//Session
var gameId;
var word;
var state;

//Ajax calls
function newGame() {
    $.ajax({
        url: "webapi/game",
        crossDomain: true,
        type: "POST",
        success: function(json) {
            gameId = json.id;
            $("#gameId").text(json.id);
            $("#word").text('_');
            $("#state").text(json.state);
            enableAddLetter();
        }
    });
}

function addLetter() {
    if (gameId != null) {
        $.ajax({
            url: "webapi/game/" + gameId + "/letter/" + $("#keyboard").val(),
            crossDomain: true,
            type: "POST",
            success: function(json) {
                state = json.state;
                word = json.word;
                $("#gameId").text(json.id);
                $("#word").text(json.word + "_");
                $("#state").text(json.state);
                $("#keyboard").val("");
                $("#keyboard").focus();
                if (json.state == 'SYSTEM_WIN') {
                    $("#state").text("SYSTEM WIN");
                    disableAddLetter();
                    endGame('You LOSE!!');
                } else if (json.state == 'PLAYER_WIN') {
                    $("#state").text("PLAYER WIN");
                    disableAddLetter();
                    endGame('You WIN!!');
                }
            }
        });
    }
}

function endGame(text) {
    $("#endedMsg1").text(text);
    $("#endedMsg2").html("The word we've been building up has been <strong>" + word + "</strong><p>Another game?</p>");
    $('#modalEndGame').modal();
}

function enableAddLetter() {
    $("#keyboard").prop('disabled', false);
    $("#keyboard").focus();
}

function disableAddLetter() {
    $("#keyboard").prop('disabled', true);
    $("#addLetterButton").prop('disabled', true);
}

