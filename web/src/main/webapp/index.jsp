<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Ghost Game</title>

        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <!-- Validator CSS -->
        <link rel="stylesheet" href="css/bootstrapValidator.min.css"/>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Icon -->        
        <link rel="icon" href="img/favicon.ico" type="image/x-icon" />

        <!-- Ghost game custom files-->
        <link href="css/ghost.css" rel="stylesheet">
        <script src="js/ghost.js"></script>
    </head>
    <body>

        <!-- HEADER -->
        <div  id="header" class="text-center">
            <div class="container-fluid">
                <div  id="brand" class="col-xs-5 col-xs-offset-2">
                    <strong>The Ghost Game</strong>
                </div>
                <div  class="col-xs-1">
                    <button id="newGameButton" class="btn btn-lg " data-toggle="modal" data-target="#modalNewGame" >
                        New game
                    </button>
                </div>
            </div>
        </div>

        <!-- GAME -->
        <div id="gameSection">
            <div id="instructions" class="container-fluid col-xs-8 col-xs-offset-2">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <p>
                            In the game of Ghost, two players take turns building up an English word from left to right. 
                            Each player adds one letter per turn. 
                        </p>
                        <p>
                            The goal is to not complete the spelling of a word: if you add a letter that completes a word (of 4+ letters),
                            or if you add a letter that produces a string that cannot be extended into a word, you lose
                        </p> 
                    </div>
                </div>

            </div>

            <div id="gameContainer" class="container">
                <div class="col-xs-3" id="systemLetter">
                    <img src="img/ghost.png" alt="Ghost image" class="img-responsive">
                </div>

                <div  id="gamePanel" class="col-xs-9">
                    <div  id="statusPanel" class="panel panel-default">
                        <div class="panel-heading">
                            <span>GAME ID</span>
                            <span id="gameId"></span>                            
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <span>   
                                        <label>STATE</label>
                                    </span>                            
                                    <span id="state" class="text-info">
                                        -
                                    </span>
                                </div>
                            </div>
                            <div class="row"> 
                                <div class="col-xs-12">
                                    <span>
                                        <label>WORD</label>                                        
                                    </span>
                                </div>
                                <div id="word" class="text-center">
                                    <strong> _ _ _ _ _ </strong>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div>
                        <form id="newLetterForm" class="form-horizontal">
                            <div class="form-group">
                                <label class="col-lg-4 control-label">Introduce your next letter</label>
                                <div class="col-lg-4">
                                    <input type="text" class="form-control" name="newLetter" id="keyboard" maxlength="1" disabled="disabled"/>
                                </div>
                                <button id="addLetterButton" class="btn-lg " onclick="addLetter()" disabled="disabled" type="submit">
                                    Add letter
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- FOOTER -->
        <div id="footer">
            <div id ="attributions">
                <div>Built with <a href="http://twitter.github.com/bootstrap/" target="_blank">Bootstrap</a>.</div>
                <div>Icons from <a href="http://glyphicons.com/" target="_blank">Glyphicons</a>.</div>
                <div>Icon made by <a href="http://www.freepik.com" title="Freepik">Freepik</a> from <a href="http://www.flaticon.com/free-icon/creepy-halloween-ghost_12394" title="Flaticon">www.flaticon.com</a>.</div>
            </div>
        </div>

        <!-- New game MODAL pop up -->
        <div id="modalNewGame" class="modal fade" role="dialog"  aria-labelledby="modalNewGameLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="modalNewGameLabel">New game</h4>
                    </div>
                    <div class="modal-body">
                        <h2>Wanna play a game?</h2>
                        <h1>Or are you <strong>scaaaaaared</strong>?</h1>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="newGame()">Play</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <!-- End game MODAL pop up -->
        <div id="modalEndGame" class="modal fade" role="dialog"  aria-labelledby="modalEndGameLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="modalEndGameLabel">Game ended</h4>
                    </div>
                    <div class="modal-body">
                        <div id="endedMsg1" class="endedMsg"></div>
                        <div id="endedMsg2" class="endedMsg"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="newGame()">Play</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>        
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.min.js"></script>
        <!-- Validator -->
        <script type="text/javascript" src="js/bootstrapValidator.min.js"></script>

        <script>
                            //Init
                            $("#keyboard").val("");
                            $('#modalNewGame').modal();

                            //validate letter
                            $(document).ready(function() {
                                $('#newLetterForm').bootstrapValidator({
                                    feedbackIcons: {
                                        valid: 'glyphicon glyphicon-ok',
                                        invalid: 'glyphicon glyphicon-remove',
                                        validating: 'glyphicon glyphicon-refresh'
                                    },
                                    submitButtons: '#addLetterButton',
                                    submitHandler: function(validator, form, submitButton) {
                                        // Do nothing
                                    },
                                    fields: {
                                        newLetter: {
                                            validators: {
                                                regexp: {
                                                    regexp: /^[a-z]+$/i,
                                                    message: 'The full name can consist of alphabetical charactersonly'
                                                }
                                            }
                                        }
                                    }
                                });
                            });
        </script>
    </body>
</html>