package com.paco.ghost.web.rest;

import com.paco.ghost.application.GameService;
import org.glassfish.hk2.utilities.binding.AbstractBinder;

/**
 *
 * @author paco
 */
public class GhostBinder extends AbstractBinder {
    
    @Override
    protected void configure() {
        bind(GameService.class).to(GameService.class);
    }
}
