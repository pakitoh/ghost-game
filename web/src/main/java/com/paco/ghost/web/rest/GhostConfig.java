package com.paco.ghost.web.rest;

import java.util.HashMap;
import java.util.Map;
import javax.ws.rs.ext.ContextResolver;
import org.glassfish.jersey.filter.LoggingFilter;
import org.glassfish.jersey.moxy.json.MoxyJsonConfig;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.spring.scope.RequestContextFilter;

/**
 *
 * @author paco
 */
public class GhostConfig extends ResourceConfig {

    public GhostConfig() {
        super();
        register(RequestContextFilter.class);
        register(LoggingFilter.class);
        register(GhostBinder.class);
        register(createMoxyJsonResolver());
        packages(true, "com.paco.ghost");
    }

    public static ContextResolver<MoxyJsonConfig> createMoxyJsonResolver() {
        final MoxyJsonConfig moxyJsonConfig = new MoxyJsonConfig();
        Map<String, String> namespacePrefixMapper = new HashMap<String, String>(1);
        namespacePrefixMapper.put("http://www.w3.org/2001/XMLSchema-instance", "xsi");
        moxyJsonConfig.setNamespacePrefixMapper(namespacePrefixMapper).setNamespaceSeparator(':');
        return moxyJsonConfig.resolver();
    }
}
