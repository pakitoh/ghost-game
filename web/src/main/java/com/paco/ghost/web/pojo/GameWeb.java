package com.paco.ghost.web.pojo;

import com.paco.ghost.domain.model.Game;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author paco
 */
@XmlRootElement
public class GameWeb {
    private String id;
    private String word;
    private String state;
    private String href;

    public GameWeb() {
    }

    public GameWeb(Game game) {
        this.id = game.getId().toString();
        this.word = game.getWord().toString();
        this.state = game.getWord().toString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }
    
}
