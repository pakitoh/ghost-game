package com.paco.ghost.web.rest;

import com.paco.ghost.application.GameService;
import com.paco.ghost.domain.model.Game;
import com.paco.ghost.domain.model.GameId;
import com.paco.ghost.domain.model.Letter;
import com.paco.ghost.web.pojo.GameWeb;
import java.net.URI;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import org.glassfish.jersey.server.JSONP;

/**
 *
 * @author paco
 */
@Path("game")
public class GameResource {

    @Context
    UriInfo uriInfo;

    @Inject
    GameService gameService;

    @POST
    @JSONP
    @Produces(MediaType.APPLICATION_JSON)
    public GameWeb executePost() {        
        Game game = gameService.createGame(); 
        GameWeb gameWeb = new GameWeb();
        gameWeb.setId(game.getId().toString());
        gameWeb.setHref(uriInfo.getAbsolutePathBuilder().path(gameWeb.getId()).build().getPath());
        gameWeb.setState(game.getState().toString());
        gameWeb.setWord(game.getWord().toString());        
        return gameWeb;
    }
     
    @GET
    @Path("{gameId}")
    @Produces(MediaType.APPLICATION_JSON)
    public GameWeb getState(@PathParam("gameId") String gameId) {
        System.out.println("GameId : " + gameId );
        //TODO: vaildate inputs
        GameWeb gameWeb = new GameWeb();
        gameWeb.setId(gameId);
        gameWeb.setHref(uriInfo.getAbsolutePathBuilder().path(gameWeb.getId()).build().getPath());        
        gameWeb.setState(gameService.getState(new GameId(gameId)).toString());
        gameWeb.setWord(gameService.getWord(new GameId(gameId)).toString());
        return gameWeb;
    }

    @POST
    @Path("{gameId}/letter/{letterToAdd}")
    @Produces(MediaType.APPLICATION_JSON)
    public GameWeb addLeter(@PathParam("gameId") String gameId, @PathParam("letterToAdd") String letterToAdd) {
        System.out.println("GameId : " + gameId );
        System.out.println("Letter : " + letterToAdd );
        //TODO: vaildate inputs (id es un juego calido y no termiando y letra)
        gameService.addLetter(
                new GameId(gameId),
                new Letter(letterToAdd.charAt(0)));
        
        GameWeb gameWeb = new GameWeb();
        gameWeb.setId(gameId);
        gameWeb.setHref(uriInfo.getAbsolutePathBuilder().path(gameWeb.getId()).build().getPath());        
        gameWeb.setState(gameService.getState(new GameId(gameId)).toString());
        gameWeb.setWord(gameService.getWord(new GameId(gameId)).toString());
        return gameWeb;
    }
}
