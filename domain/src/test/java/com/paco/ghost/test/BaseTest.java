package com.paco.ghost.test;

import org.junit.Rule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

/**
 *
 * @author paco
 */
public class BaseTest {

    @Rule
    public TestWatcher watcher = new TestWatcher() {

        @Override
        protected void starting(Description description) {
            System.out.println("Starting test: " + description.toString());
        }
    };
}
