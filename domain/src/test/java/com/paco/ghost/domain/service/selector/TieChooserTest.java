package com.paco.ghost.domain.service.selector;

import com.paco.ghost.test.BaseMockTest;
import java.util.Random;
import org.jmock.Expectations;
import org.junit.Before;
import org.junit.Test;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

/**
 *
 * @author paco
 */
public class TieChooserTest extends BaseMockTest {

    //SUT
    TieChooser chooser;
    //Collaborators
    Random rand;

    @Test
    public void selectOneShouldCallRandom() {
        rand = context.mock(Random.class);
        chooser = new TieChooser(rand);
        final int range = 5;
        context.checking(new Expectations() {
            {
                oneOf(rand).nextInt(range);
            }
        });
        chooser.selectOne(range);
        context.assertIsSatisfied();
    }
    
    @Test
    public void selectOneShouldSelectInRange() {
        chooser = new TieChooser(new Random()); 
        final int range = 5;
        int chose = chooser.selectOne(range);
        assertThat(chose, is(greaterThanOrEqualTo(0)));
        assertThat(chose, is(lessThan(range)));
    }
}
