package com.paco.ghost.domain.model;

import com.paco.ghost.test.BaseTest;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author paco
 */
public class DictionaryTest extends BaseTest {

    //SUT
    private Dictionary dictionary;

    @Before
    public void setUp() {
        dictionary = new Dictionary();
        dictionary.add(new Word("aa"));
        dictionary.add(new Word("aal"));
        dictionary.add(new Word("aalii"));
        dictionary.add(new Word("aaliis"));
    }

    @Test
    public void containsAaShouldReturnTrue() {
        assertThat(dictionary.contains(new Word("aa")), is(true));
    }

    @Test
    public void containsAbShouldReturnFalse() {
        assertThat(dictionary.contains(new Word("ab")), is(false));
    }

    @Test
    public void iterateShouldHabilitateToIterateOverWords() {
        Iterator<Word> iterator = dictionary.iterator();
        int count = 0;
        Set<Word> words = new HashSet<Word>();
        while (iterator.hasNext()) {
            Word word = iterator.next();
            words.add(word);
            count++;
            assertThat(dictionary.contains(word), is(true));
        }
        assertThat(count, is(dictionary.size()));        
               
        assertThat(words.contains(new Word("aa")), equalTo(true));
        assertThat(words.contains(new Word("aal")), equalTo(true));
        assertThat(words.contains(new Word("aalii")), equalTo(true));
        assertThat(words.contains(new Word("aaliis")), equalTo(true));
    }
   
    @Test
    public void addWordFutureShouldIncludeWordFutureInDictionary() {
        Word word = new Word("future");
        dictionary.add(word);
        assertThat(dictionary.contains(word), equalTo(true));
    }

    @Test
    public void sizeShouldReturn4() {        
        assertThat(dictionary.size(), equalTo(4));
    }

    @Test
    public void findWordsStartingWithFuturShouldReturnEmpty() {
        Word prefix = new Word("futur");
        Dictionary words = dictionary.findWordsStartingWith(prefix);
        assertThat(words.size(), equalTo(0));    
    }

    @Test
    public void findWordsStartingWithAalShouldReturn3() {
        Word prefix = new Word("aal");
        Dictionary words = dictionary.findWordsStartingWith(prefix);
        assertThat(words.size(), equalTo(3));    
    }
}
