package com.paco.ghost.domain.service.selector;

import com.paco.ghost.domain.model.Dictionary;
import com.paco.ghost.test.BaseMockTest;
import org.jmock.Expectations;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author paco
 */
public class PlayToWinSelectorTest extends BaseMockTest {

    //SUT
    private PlayToWinSelector selector;
    //Collaborators
     TieChooser tieChooser;
     //Aux
     Dictionary winnerCandidates;
     
    @Before
    public void setUp() {
        tieChooser = context.mock(TieChooser.class);        
        selector = new PlayToWinSelector(tieChooser);
        winnerCandidates = context.mock(Dictionary.class);                
    }    

    @Test
    public void selectPlayShouldCallCollaborators() {
        final int range = 1;
        final int selectedIndex = 1;
        context.checking(new Expectations() {
            {
                oneOf(winnerCandidates).size();
                will(returnValue(range));
                
                oneOf(tieChooser).selectOne(range);
                will(returnValue(selectedIndex));
                
                oneOf(winnerCandidates).get(selectedIndex);                
            }
        });
        selector.selectPlay(winnerCandidates);
        context.assertIsSatisfied();
    }
    
}
