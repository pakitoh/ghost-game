package com.paco.ghost.domain.factory;

import com.paco.ghost.domain.model.Dictionary;
import com.paco.ghost.domain.model.Game;
import com.paco.ghost.domain.service.NextLetterSelector;
import com.paco.ghost.test.BaseMockTest;
import org.jmock.Expectations;
import org.junit.Test;

/**
 *
 * @author paco
 */
public class GameFactoryTest extends BaseMockTest {

    //SUT
    private GameFactory factory;

    @Test
    public void createGameShouldCallFactoryId() {
        final IdFactory idFactory = context.mock(IdFactory.class);
        final Dictionary dictionary = context.mock(Dictionary.class);
        final NextLetterSelector nextLetterSelector = context.mock(NextLetterSelector.class);
        factory = new GameFactory(idFactory, dictionary, nextLetterSelector);
        context.checking(new Expectations() {
            {
                oneOf(idFactory).createId();                
            }
        });
        Game game = factory.createGame();
        context.assertIsSatisfied();
    }
}
