package com.paco.ghost.domain.model;

import com.paco.ghost.domain.service.NextLetterSelector;
import com.paco.ghost.test.BaseMockTest;
import org.jmock.Expectations;
import static org.jmock.Expectations.returnValue;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author paco
 */
public class GameTest extends BaseMockTest {

    //SUT
    private Game game;
    //Collaborators
    private GameId gameId;
    private Dictionary dictionary;
    private NextLetterSelector selector;    
    
    @Before
    public void setUp() {
        gameId = context.mock(GameId.class);
        dictionary = context.mock(Dictionary.class);
        selector = context.mock(NextLetterSelector.class);        
        game = new Game(gameId, dictionary, selector);
    }
    
    @Test
    public void addLetterShouldCallCollaborators() {
        Character character = 'f';        
        final int numCandidates = 5;
        final Letter letter = new Letter(character);
        context.checking(new Expectations() {
            {
                 atLeast(1).of(dictionary).contains(game.getWord());
                 will(returnValue(false));
                 
                atLeast(1).of(dictionary).findWordsStartingWith(game.getWord());
                will(returnValue(dictionary));

                atLeast(1).of(dictionary).size();
                will(returnValue(numCandidates));
                
                oneOf(selector).selectNextLetter(game.getWord());
            }
        });
        game.addLetter(letter);
        context.assertIsSatisfied();
    }

    @Test
    public void isEndedShouldCallCollaborators() {                   
        context.checking(new Expectations() {
            {
                oneOf(dictionary).contains(game.getWord());
                oneOf(dictionary).findWordsStartingWith(game.getWord());
            }
        });
        game.isEnded();
        context.assertIsSatisfied();
    }
}
