package com.paco.ghost.domain.factory;

import com.paco.ghost.test.BaseTest;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author paco
 */
public class IdFactoryTest extends BaseTest {

    //SUT
    private IdFactory factory;

    @Before
    public void setUp() {
        factory = new IdFactory();
    }

    @Test
    public void testCreateIdShouldCreateRandomId() {
        String id1 = factory.createId();
        String id2 = factory.createId();

        assertThat(id1, not(equalTo(id2)));
    }
}
