package com.paco.ghost.domain.model;

import com.paco.ghost.test.BaseTest;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author paco
 */
public class WordTest extends BaseTest {

    //SUT
    private Word word;

    @Before
    public void setUp() {
        word = new Word();
    }

    @Test
    public void addFToAnEmptyWordShouldConvertTheWordToF() {
        word.addLetter(new Letter('f'));
        assertThat(word.toString(), equalTo("f"));
    }

    @Test
    public void addUpperFToAnEmptyWordShouldConvertTheWordToLowerF() {
        word.addLetter(new Letter('F'));
        assertThat(word.toString(), equalTo("f"));
    }

    @Test
    public void addUToTheFWordShouldConvertTheWordToFU() {
        word.addLetter(new Letter('F'));
        word.addLetter(new Letter('U'));
        assertThat(word.toString(), equalTo("fu"));
    }
}
