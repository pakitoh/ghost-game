package com.paco.ghost.domain.service;

import com.paco.ghost.domain.model.Dictionary;
import com.paco.ghost.domain.model.Word;
import com.paco.ghost.domain.service.selector.PlayToLoseSelector;
import com.paco.ghost.domain.service.selector.PlayToWinSelector;
import com.paco.ghost.domain.service.selector.WinnerCandidatesCalculator;
import com.paco.ghost.test.BaseMockTest;
import org.jmock.Expectations;
import static org.jmock.Expectations.returnValue;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author paco
 */
public class NextLetterSelectorTest extends BaseMockTest {

    //SUT
    private NextLetterSelector selector;
    //Collaborators
    private Dictionary dictionary;
    private WinnerCandidatesCalculator candidatesCalculator;
    private PlayToWinSelector playToWinSelector;
    private PlayToLoseSelector playToLoseSelector;
    //Aux
    private Word word;
    private Dictionary wordsWithSamePrefix;
    private Dictionary winnerCandidates;

    @Before
    public void setUp() {
        word = new Word();
        dictionary = context.mock(Dictionary.class, "dictionary");
        wordsWithSamePrefix = context.mock(Dictionary.class, "wordsWithSamePrefix");
        winnerCandidates = context.mock(Dictionary.class, "winnerCandidates");
        candidatesCalculator = context.mock(WinnerCandidatesCalculator.class);
        playToWinSelector = context.mock(PlayToWinSelector.class);
        playToLoseSelector = context.mock(PlayToLoseSelector.class);
        selector = new NextLetterSelector(dictionary, candidatesCalculator, playToWinSelector, playToLoseSelector);
    }

    @Test
    public void selectNextLetterToWinShouldCallCollaborators() {
        final int numCandidates = 1;
        addBaseExpectations(numCandidates);
        context.checking(new Expectations() {
            {
                oneOf(playToWinSelector).selectPlay(winnerCandidates);
            }
        });        
        selector.selectNextLetter(word);
        context.assertIsSatisfied();
    }

    @Test
    public void selectNextLetterToLoseShouldCallCollaborators() {
        final int numCandidates = 0;
        addBaseExpectations(numCandidates);
        context.checking(new Expectations() {
            {
                oneOf(playToLoseSelector).selectPlay(wordsWithSamePrefix);
            }
        });        
        selector.selectNextLetter(word);
        context.assertIsSatisfied();
    }    

    private void addBaseExpectations(final int numCandidates) {
        context.checking(new Expectations() {
            {
                oneOf(dictionary).findWordsStartingWith(word);
                will(returnValue(wordsWithSamePrefix));

                oneOf(candidatesCalculator).calculateCandidates(wordsWithSamePrefix);
                will(returnValue(winnerCandidates));

                oneOf(winnerCandidates).size();
                will(returnValue(numCandidates));
            }
        });
    }
}
