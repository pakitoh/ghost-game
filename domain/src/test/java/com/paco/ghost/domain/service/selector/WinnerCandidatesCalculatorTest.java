package com.paco.ghost.domain.service.selector;

import com.paco.ghost.domain.model.Dictionary;
import com.paco.ghost.domain.model.Word;
import com.paco.ghost.test.BaseTest;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author paco
 */
public class WinnerCandidatesCalculatorTest extends BaseTest {

    //SUT
    WinnerCandidatesCalculator calculator;
    //Collaborators
    Dictionary wordsWithSamePrefix;
    
    @Before
    public void setUp() {
        calculator = new WinnerCandidatesCalculator();
        wordsWithSamePrefix = new Dictionary();
        wordsWithSamePrefix.add(new Word("aa"));
        wordsWithSamePrefix.add(new Word("aal"));
        wordsWithSamePrefix.add(new Word("aalii"));
        wordsWithSamePrefix.add(new Word("aaliis"));
    }

    @Test
    public void calculateCandidatesShouldNotIncludeAa() {
        Dictionary candidates = calculator.calculateCandidates(wordsWithSamePrefix);
        assertThat(candidates.contains(new Word("aa")), equalTo(false));
    }
    
    @Test
    public void calculateCandidatesShouldIncludeAal() {
        Dictionary candidates = calculator.calculateCandidates(wordsWithSamePrefix);
        assertThat(candidates.contains(new Word("aal")), equalTo(true));
    }
    
    @Test
    public void calculateCandidatesShouldIncludeAalii() {
        Dictionary candidates = calculator.calculateCandidates(wordsWithSamePrefix);
        assertThat(candidates.contains(new Word("aalii")), equalTo(true));
    }

    @Test
    public void calculateCandidatesShouldNotIncludeAaliis() {
        Dictionary candidates = calculator.calculateCandidates(wordsWithSamePrefix);
        assertThat(candidates.contains(new Word("aaliis")), equalTo(false));
    }    
}
