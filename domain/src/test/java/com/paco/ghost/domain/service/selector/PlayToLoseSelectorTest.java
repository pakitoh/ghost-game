package com.paco.ghost.domain.service.selector;

import com.paco.ghost.domain.service.selector.PlayToLoseSelector;
import com.paco.ghost.domain.service.selector.TieChooser;
import com.paco.ghost.domain.model.Dictionary;
import com.paco.ghost.domain.model.Word;
import com.paco.ghost.test.BaseMockTest;
import org.jmock.Expectations;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author paco
 */
public class PlayToLoseSelectorTest extends BaseMockTest {

    //SUT
    private PlayToLoseSelector selector;
    //Collaborators
    TieChooser tieChooser;
    //Aux
    Dictionary wordsWithSamePrefix;

    @Before
    public void setUp() {
        tieChooser = context.mock(TieChooser.class);
        selector = new PlayToLoseSelector(tieChooser);
        wordsWithSamePrefix = new Dictionary();
        wordsWithSamePrefix.add(new Word("aa"));
        wordsWithSamePrefix.add(new Word("aal"));
        wordsWithSamePrefix.add(new Word("aalii"));
        wordsWithSamePrefix.add(new Word("aaliis"));
    }

    @Test
    public void selectPlayShouldCallCollaborators() {
        final int range = 1;
        final int selectedIndex = 0;
        context.checking(new Expectations() {
            {
                oneOf(tieChooser).selectOne(range);
                will(returnValue(selectedIndex));
            }
        });
        selector.selectPlay(wordsWithSamePrefix);
        context.assertIsSatisfied();
    }
}
