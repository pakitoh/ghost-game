package com.paco.ghost.domain.service.selector;

import com.paco.ghost.domain.model.Dictionary;
import com.paco.ghost.domain.model.Word;
import java.util.Iterator;
import javax.inject.Named;
import org.apache.log4j.Logger;

/**
 *
 * @author paco
 */
@Named
public class WinnerCandidatesCalculator {

    private final Logger logger = Logger.getLogger(WinnerCandidatesCalculator.class);

    private boolean isPlayer = false;
    
    public WinnerCandidatesCalculator() {
    }

    public WinnerCandidatesCalculator(boolean isPlayer) {
        this.isPlayer = isPlayer;
    }

    
    public Dictionary calculateCandidates(Dictionary wordsWithSamePrefix ) {
        Dictionary candidates = new Dictionary();
        for (Iterator<Word> itWords = wordsWithSamePrefix.iterator(); itWords.hasNext();) {
            Word wordWithSamePrefix = itWords.next();
            if (isChoosible(wordWithSamePrefix)) {
                candidates.add(wordWithSamePrefix);
            }
        }
        logger.debug("Winner candidates: " + candidates.toString());
        return candidates;
    }

    private boolean isChoosible(Word candidate) {
        if(isPlayer) {
           return isEven(candidate);
        }
        return isOdd(candidate);
    }
    
    private boolean isEven(Word candidate) {
        return (candidate.length() % 2 == 0);
    }

    private boolean isOdd(Word candidate) {
        return (candidate.length() % 2 != 0);
    }
}
