package com.paco.ghost.domain.service;

import com.paco.ghost.domain.model.Dictionary;
import com.paco.ghost.domain.model.Letter;
import com.paco.ghost.domain.model.Word;
import com.paco.ghost.domain.service.selector.PlayToLoseSelector;
import com.paco.ghost.domain.service.selector.PlayToWinSelector;
import com.paco.ghost.domain.service.selector.WinnerCandidatesCalculator;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.log4j.Logger;

/**
 *
 * @author paco
 */
@Named
public class NextLetterSelector {

    private final Logger logger = Logger.getLogger(NextLetterSelector.class);

    @Inject
    private final Dictionary dictionary;

    @Inject
    private final WinnerCandidatesCalculator winnerCandidatesCalculator;

    @Inject
    private final PlayToWinSelector playToWinSelector;

    @Inject
    private final PlayToLoseSelector playToLoseSelector;

    public NextLetterSelector(Dictionary dictionary, WinnerCandidatesCalculator winnerCandidatesCalculator, PlayToWinSelector playToWinSelector, PlayToLoseSelector playToLoseSelector) {
        this.dictionary = dictionary;
        this.winnerCandidatesCalculator = winnerCandidatesCalculator;
        this.playToWinSelector = playToWinSelector;
        this.playToLoseSelector = playToLoseSelector;
    }

    public Letter selectNextLetter(final Word word) {
        logger.debug("Looking for the letter after the word " + word.toString());
        Dictionary wordsWithSamePrefix = dictionary.findWordsStartingWith(word);
        logger.debug("Words with the same prefix: " + wordsWithSamePrefix.toString());
        Dictionary winnerCandidates = winnerCandidatesCalculator.calculateCandidates(wordsWithSamePrefix);
        Word wordChoose;
        if (canSystemWin(winnerCandidates)) {
            wordChoose = playToWinSelector.selectPlay(winnerCandidates);
        } else {
            wordChoose = playToLoseSelector.selectPlay(wordsWithSamePrefix);
        }
        Letter letterChoose = new Letter(wordChoose.getLetter(word.length()));
        logger.info("The selected letter is " + letterChoose);
        return letterChoose;
    }

    private boolean canSystemWin(Dictionary winnerCandidates) {
        final boolean canWin = winnerCandidates.size() > 0;
        logger.debug("The system can win?" + canWin);
        return canWin;
    }
}
