package com.paco.ghost.domain.service.selector;

import java.util.Random;
import javax.inject.Named;
import org.apache.log4j.Logger;

/**
 *
 * @author paco
 */
@Named
public class TieChooser {

    private final Logger logger = Logger.getLogger(TieChooser.class);
    private final Random rand;

    public TieChooser(Random rand) {
        this.rand = rand;
    }
    
    public int selectOne(int numOptions) {
        int selectedOne = rand.nextInt(numOptions);
        logger.debug("Selected option " + selectedOne);
        return selectedOne;
    }
}
