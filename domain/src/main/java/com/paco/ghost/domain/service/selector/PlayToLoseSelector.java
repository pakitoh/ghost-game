package com.paco.ghost.domain.service.selector;

import com.paco.ghost.domain.model.Dictionary;
import com.paco.ghost.domain.model.Word;
import com.paco.ghost.domain.service.NextLetterSelector;
import java.util.Iterator;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.log4j.Logger;

/**
 *
 * @author paco
 */
@Named
public class PlayToLoseSelector {

    private final Logger logger = Logger.getLogger(NextLetterSelector.class);

    @Inject
    private final TieChooser tieChooser;

    public PlayToLoseSelector(TieChooser tieChooser) {
        this.tieChooser = tieChooser;
    }

    public Word selectPlay(Dictionary wordsWithSamePrefix) {
         Dictionary longestsWords =  calculateLongestWords(wordsWithSamePrefix);
        logger.debug("Longest candidates: " + longestsWords);
        int index = tieChooser.selectOne(longestsWords.size());
        logger.debug("We choose index " + index);
        Word word = longestsWords.get(index);
        logger.debug("We chooose the word " + word);
        return word;
    }

    public Dictionary calculateLongestWords(Dictionary wordsWithSamePrefix) {
        Dictionary longestWords = new Dictionary();
        int maxLength = 0;
        for (Iterator<Word> itWords = wordsWithSamePrefix.iterator(); itWords.hasNext();) {
            Word word = itWords.next();
            if (word.length() > maxLength) {
                maxLength = word.length();
            }
        }
        for (Iterator<Word> itWords = wordsWithSamePrefix.iterator(); itWords.hasNext();) {
            Word word = itWords.next();
            if (word.length() == maxLength) {
                longestWords.add(word);
            }
        }
        return longestWords;
    }
}
