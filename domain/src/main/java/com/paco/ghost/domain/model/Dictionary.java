package com.paco.ghost.domain.model;

import java.util.Iterator;
import javax.inject.Named;
import org.ardverk.collection.PatriciaTrie;
import org.ardverk.collection.StringKeyAnalyzer;

/**
 *
 * @author paco
 */
@Named
public class Dictionary {

    private final PatriciaTrie<String, Word> words;

    public Dictionary() {
        words = new PatriciaTrie<String, Word>(StringKeyAnalyzer.CHAR);
    }

    private Dictionary(PatriciaTrie<String, Word> words) {
        this.words = words;
    }

    public boolean contains(final Word word) {
        return words.containsKey(word.toString());
    }

    public Iterator<Word> iterator() {
        return words.values().iterator();
    }

    public void add(Word word) {
        words.put(word.toString(), word);
    }

    public int size() {
        return words.size();
    }

    public Dictionary findWordsStartingWith(final Word prefix) {
        return new Dictionary(new PatriciaTrie<String, Word>(StringKeyAnalyzer.CHAR, words.prefixMap(prefix.toString())));
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder("{");
        for (String word : words.keySet()) {
            builder.append("[");
            builder.append(word);
            builder.append("]");
        }
        builder.append("}");
        return builder.toString();
    }

    public Word get(int index) {
        String key = words.keySet().toArray(new String[ words.keySet().size()])[index];
        return words. get(key);
    }
}
