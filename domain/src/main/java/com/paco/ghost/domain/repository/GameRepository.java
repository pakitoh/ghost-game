package com.paco.ghost.domain.repository;

import com.paco.ghost.domain.model.Game;
import com.paco.ghost.domain.model.GameId;

/**
 *
 * @author paco
 */
public interface GameRepository {

    void addGame(final Game game);

    Game getGame(final GameId gameId);
}
