package com.paco.ghost.domain.factory;

import com.paco.ghost.domain.model.Dictionary;
import com.paco.ghost.domain.model.Game;
import com.paco.ghost.domain.model.GameId;
import com.paco.ghost.domain.service.NextLetterSelector;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author paco
 */
@Named
public class GameFactory {

    @Inject
    private final IdFactory idFactory;
    
    @Inject
    private final Dictionary dictionary;
    
    @Inject
    private final NextLetterSelector nextLetterSelector;

    public GameFactory(IdFactory idFactory, Dictionary dictionary, NextLetterSelector nextLetterSelector) {
        this.idFactory = idFactory;
        this.dictionary = dictionary;
        this.nextLetterSelector = nextLetterSelector;
    }

    public Game createGame() {
        return new Game(
                new GameId(idFactory.createId()),
                dictionary, 
                nextLetterSelector);
    }
}
