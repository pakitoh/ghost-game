package com.paco.ghost.domain.model;

/**
 *
 * @author paco
 */
public class Word {

    private String word;

    public Word() {
        word = "";
    }

    public Word(final String word) {
        this.word = word;
    }

    public void addLetter(final Letter letter) {
        word += letter.toChar();
    }

    @Override
    public String toString() {
        return word;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 47 * hash + (this.toString() != null ? this.toString().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        boolean isEqual = false;
        final Word otherWord = (Word) obj;
        if (this.toString().equals(otherWord.toString())) {
            isEqual = true;
        }
        return isEqual;
    }

    public int length() {
        return word.length();
    }

    public Word substring(int i, int i0) {
        return new Word(word.substring(i, i0));
    }

    public char getLetter(int length) {
        return word.charAt(length);
    }
}
