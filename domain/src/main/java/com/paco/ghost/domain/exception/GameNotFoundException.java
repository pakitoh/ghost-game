package com.paco.ghost.domain.exception;

/**
 *
 * @author paco
 */
public class GameNotFoundException extends RuntimeException  {

    public GameNotFoundException(final String string) {
        super(string);
    }    
}
