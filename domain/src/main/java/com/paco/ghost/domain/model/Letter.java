package com.paco.ghost.domain.model;

/**
 *
 * @author paco
 */
public class Letter {
    private final Character letter;

    public Letter(final Character letter) {
        //lower case
        this.letter = letter.toString().toLowerCase().charAt(0);
    }
    
    public Character toChar() {
        return letter;
    }

    @Override
    public String toString() {
        return letter.toString();
    }    
}