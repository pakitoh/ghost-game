package com.paco.ghost.domain.model;

import com.paco.ghost.domain.service.NextLetterSelector;

/**
 *
 * @author paco
 */
public class Game {
    public enum State {PLAYING, SYSTEM_WIN, PLAYER_WIN }

    private final GameId id;
    private final Word word;
    private final Dictionary dictionary;
    private final NextLetterSelector nextLetterSelector;
    private State state;
    
    public Game(final GameId id, final Dictionary dictionary, NextLetterSelector nextLetterSelector) {
        this.id = id;
        this.dictionary = dictionary;
        this.nextLetterSelector = nextLetterSelector;
        word = new Word();
        state = State.PLAYING;
    }

    public GameId getId() {
        return id;
    }

    public Word getWord() {
        return word;
    }

    public State getState() {
        return state;
    }

    @Override
    public String toString() {
        return id.toString();
    }

    public void addLetter(Letter playerLetter) {
        word.addLetter(playerLetter);
        if (isEnded()) {            
            state = State.SYSTEM_WIN;
        } else {
            Letter systemLetter = nextLetterSelector.selectNextLetter(word);            
            word.addLetter(systemLetter);
            if (isEnded()) {
                state = State.PLAYER_WIN;
            }
        }
    }

    public boolean isEnded() {
        return dictionary.contains(getWord())
                || !isThereWordsInDictionaryWithActualWordAsPrefix();
    }

    private boolean isThereWordsInDictionaryWithActualWordAsPrefix() {
        return dictionary.findWordsStartingWith(getWord()).size() > 0;
    }
}
