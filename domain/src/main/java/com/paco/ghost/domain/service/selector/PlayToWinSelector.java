package com.paco.ghost.domain.service.selector;

import com.paco.ghost.domain.model.Dictionary;
import com.paco.ghost.domain.model.Word;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.log4j.Logger;

/**
 *
 * @author paco
 */
@Named
public class PlayToWinSelector {
    
    private final Logger logger = Logger.getLogger(PlayToWinSelector.class);

    @Inject
    private final TieChooser tieChooser;

    public PlayToWinSelector(TieChooser tieChooser) {
        this.tieChooser = tieChooser;
    }
    
    public Word selectPlay( Dictionary winnerCandidates) {
        int index = tieChooser.selectOne(winnerCandidates.size());
        Word word = winnerCandidates.get(index);
        logger.debug("Selected word: " + word);
        return word;               
    }
}
