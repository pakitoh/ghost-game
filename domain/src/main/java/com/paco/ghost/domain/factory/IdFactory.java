package com.paco.ghost.domain.factory;

import javax.inject.Named;

/**
 *
 * @author paco
 */
@Named
public class IdFactory {
    
    public String createId() {
        return java.util.UUID.randomUUID().toString();
    }
}
