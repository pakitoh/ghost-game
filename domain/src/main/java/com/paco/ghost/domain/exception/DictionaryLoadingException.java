package com.paco.ghost.domain.exception;

/**
 *
 * @author paco
 */
public class DictionaryLoadingException extends RuntimeException  {

    public DictionaryLoadingException(final String message) {
        super(message);
    }    

    public DictionaryLoadingException(final String message, final Throwable throwable) {
        super(message, throwable);
    }    
}
